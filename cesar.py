from tkinter import *
from tkinter.messagebox import showinfo
import string

# FONCTION QUI ENCODE LE MESSAGE
def encode(message, decalage):
    messageTraite=""
    for char in message:
        asciiNombre = ord(char)
        if(circular_intvar.get() == 1):
            asciiNombre = circulariser(char,decalage)
        else:
            asciiNombre += decalage
        messageTraite = (''.join([messageTraite, chr(asciiNombre)]))
    return messageTraite

# FONCTION QUI DECODE LE MESSAGE
def decode(message, decalage):
    decalage = -decalage
    messageTraite = encode(message, decalage)
    return messageTraite

#FONCTION QUI DOIT CIRCULARISER LE CARACTERE
def circulariser(caractere,cle):
    cle_temporaire = cle % 26
    if(caractere in string.ascii_uppercase):
        dict_circularise = string.ascii_uppercase[ord(caractere)-ord("A"):] + string.ascii_uppercase[:ord(caractere)-ord("A")]
        asciiNombre = ord(dict_circularise[cle_temporaire])
    elif(caractere in string.ascii_lowercase):
        dict_circularise = string.ascii_lowercase[ord(caractere)-ord("a"):] + string.ascii_lowercase[:ord(caractere)-ord("a")]
        asciiNombre = ord(dict_circularise[cle_temporaire])
    else:
        asciiNombre = ord(caractere)
        asciiNombre += cle
    return asciiNombre
        
# FONCTION QUI PERMET DE CHOISIR ENTRE LE CODAGE ET LE DECODAGE
def afficher_message(encode_flag):
    text_out.delete(0.0,END)
    if(encode_flag == True):
        text_out.insert(END,encode(text_entry.get(0.0,END).strip(), int(key_entry.get())))
    else:
        text_out.insert(END,decode(text_entry.get(0.0,END).strip(), int(key_entry.get())))

# PROGRAMME PRINCIPAL
frame = Tk()

label_entry = Label(frame,text = "Entrez votre message")
label_key = Label(frame,text = "Entrez la valeur de clé :")
label_circular = Label(frame,text = "Chiffrement circulaire")
label_out = Label(frame,text="Message Traité")

key_entry = Entry(frame)
text_entry = Text(frame, height = 15)
text_out = Text(frame, height = 15)

circular_intvar = IntVar()
circular_intvar.set(0)
circular_button = Checkbutton(frame, variable = circular_intvar)

encode_button = Button(frame, text="Encoder", command = lambda : afficher_message(True))
decode_button = Button(frame, text="Décoder", command = lambda : afficher_message(False))

# MISE EN PAGE DE LA FENETRE
label_entry.grid(row = 0,columnspan = 4)
text_entry.grid(row = 1, columnspan = 4)
label_key.grid(row = 2, column = 1)
key_entry.grid(row = 2, column = 2)
circular_button.grid(row = 3, column = 1)
label_circular.grid(row = 3, column = 2)
label_out.grid(row = 4,columnspan = 4)
text_out.grid(row = 5, columnspan = 4)
encode_button.grid(row = 7,column = 1)
decode_button.grid(row = 7,column = 2)

# POUR LA REDIMENSION
frame.rowconfigure(0, weight = 1)
frame.rowconfigure(1, weight = 10)
frame.rowconfigure(2, weight = 1)
frame.rowconfigure(3, weight = 1)
frame.rowconfigure(4, weight = 1)
frame.rowconfigure(5, weight = 10)
frame.rowconfigure(6, weight = 1)
frame.rowconfigure(7, weight = 1)

frame.columnconfigure(0, weight=1)
frame.columnconfigure(1, weight=1)
frame.columnconfigure(2, weight=1)
frame.columnconfigure(3, weight=1)

frame.mainloop()