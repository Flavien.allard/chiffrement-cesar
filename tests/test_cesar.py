import unittest
from cesar import encode
from cesar import decode

class TestEncodageCesarFunctions(unittest.TestCase):

    def test_encode(self):
        self.assertEqual(encode("fghij",1),"ghijk")
        self.assertEqual(encode("abcde",8),"ijklm")

    def test_decode(self):
        self.assertEqual(decode("ijklm",8),"abcde")